- check color #ff4518 (current Fab Lab ULB color on http://fablab-ulb.be/) 
- make it multilingual
- put a TOC at to of each page
- put fonts, javascript libs etc. in "assets" folder with proper conditional hugo compilation (see https://digitaldrummerj.me/hugo-add-copy-code-snippet-button/#add-css-and-javascript-files-to-site)
- add a header-link on each h2, h3 etc. like in https://natclark.com/tutorials/hugo-linked-headers/ or in the hugo documentation (eg https://gohugo.io/installation/linux/)
- Add a "copy code" snippet button https://digitaldrummerj.me/hugo-add-copy-code-snippet-button/
- Copy latex style section conditionnaly? With something like (from chatGPT) : 
- Cleaner show/hide for qrCodes

```
[params]
latex = true

[[module.imports]]
path = "github.com/gohugoio/hugo-modules/copy"

  [[module.imports.mounts]]
  source = "static/latex"
  target = "static/latex"
  [module.imports.mounts.options]
  if = " .Site.Params.latex "
```
