+++
title = "Home"
fa = "house-chimney-user"
faVariant = "solid"
+++

## Home page layout

An customized version of [ \\( \LaTeX now\\) ](https://latex.now.sh/) has been adapted to this template.

To activate (or deactivate) the \\( \LaTeX \\) style on this website, all you have to do is change the `Params.latexStyle` boolean value (set it `true` or `false`) in the `config.toml` file in the root project.


## Font Awesome

[Font Awesome <i class="fa-solid fa-font-awesome"></i>](https://fontawesome.com) is supported by this template.


To activate (or deactivate) the font awesome support on this website, all you have to do is change the `Params.fontAwesome` boolean value (set it `true` or `false`) in the `config.toml` file in the root project.

To use it, just copy-paste the corresponding html code from [https://fontawesome.com](https://fontawesome.com) (limit research to "free" icons). Eg, to get the "<i class="fa-solid fa-code"></i>" icon, type : 


```html
<i class="fa-solid fa-code"></i>
```

Pages parameters also accept two variables : 
- `fa`
- `faVariant`

that allows you to put an icon in the title. The two variables must be set properly.

For example put : 

```toml
+++
title = "home"
fa = "house"
faVariant = "solid"
+++
```

to put a "`fa-solid fa-house`" = <i class="fa-solid fa-house"></i> in the title of your page.


## MathJax

[MathJax](https://www.mathjax.org/) allows beautiful \\( \LaTeX \\) in MD/HTML!

$$\int_{a}^{b} x^2 dx$$

When \\(a \ne 0\\), there are two solutions to \\(ax^2 + bx + c = 0\\) and they are
$$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$



For inline forumlas, use <code>\\\\(, \\\\)</code> opening/closure in your markdown. For display formulas, use <code>$$ $$</code>.

## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading


## Horizontal Rules

___

---

***

## Emphasis

**This is bold text**

__This is bold text__

*This is italic text*

_This is italic text_

~~Strikethrough~~


## Blockquotes


> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.


## Lists

Unordered

+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!

Ordered

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa


1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`

Start numbering with offset:

57. foo
1. bar


## Code

Inline `code`

Block code "fences"

```
Sample text here...
```

Syntax highlighting

``` js
var foo = function (bar) {
  return bar++;
};

console.log(foo(5));
```

## Tables

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

Right aligned columns

| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |


## Links

[link text](http://dev.nodeca.com)

[link with title](http://nodeca.github.io/pica/demo/ "title text!")

Autoconverted link https://github.com/nodeca/pica (enable linkify to see)


## Images

![Minion](https://octodex.github.com/images/minion.png)
![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")

Like links, Images also have a footnote style syntax

![Alt text][id]

With a reference later in the document defining the URL location:

[id]: https://octodex.github.com/images/dojocat.jpg  "The Dojocat"
