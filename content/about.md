+++
title = "About"
fa = "address-card"
faVariant = "regular"
weight = 10
+++

## About me

<div class="row">

  <div class="col-lg-3 m-auto">
    <img src="img/portrait.svg" width="300"><br>
  </div>

  <div class="col-lg-6 m-auto">
  This page tells about me and who am I.


  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae placerat libero. Etiam non felis eros. Morbi aliquet ornare elementum. Maecenas sagittis enim mauris, eu cursus metus efficitur vel. Donec tempus purus ut est malesuada tempor. Donec a magna quis ex ultrices rhoncus. Aenean congue justo at dolor semper sollicitudin. Fusce faucibus leo vitae lectus tristique ullamcorper. Fusce dictum odio nec lacus suscipit, quis euismod nisi venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam non lacinia felis, nec egestas nulla. Etiam condimentum neque ut volutpat feugiat. Aliquam enim dui, lobortis luctus consequat non, venenatis quis tortor. Proin diam est, condimentum lacinia ornare vel, condimentum nec libero. Curabitur iaculis sem pretium commodo ultricies. Sed et commodo nisi, ultricies varius velit.

  </div>


</div>



## Our team


<div class="row justify-content-center">
    <div class="col-sm-3">
        <a href="http://example.com" target="_blank">
            <div class="team-member">
                <img class="mx-auto rounded-circle" src="img/portrait.svg" alt="" style="max-width:255px;">
                <h4>Arsène Lupin</h4>
                <p class="text-muted">My lab</p>
            </div>
        </a>
    </div>
    <div class="col-sm-3">
        <a href="http://example.com" target="_blank">
            <div class="team-member">
                <img class="mx-auto rounded-circle" src="img/portrait.svg" alt="" style="max-width:255px;">
                <h4>Arsène Lupin</h4>
                <p class="text-muted">My lab</p>
            </div>
        </a>
    </div>
    <div class="col-sm-3">
        <a href="http://example.com" target="_blank">
            <div class="team-member">
                <img class="mx-auto rounded-circle" src="img/portrait.svg" alt="" style="max-width:255px;">
                <h4>Arsène Lupin</h4>
                <p class="text-muted">My lab</p>
            </div>
        </a>
    </div>
    <div class="col-sm-3">
        <a href="http://example.com" target="_blank">
            <div class="team-member">
                <img class="mx-auto rounded-circle" src="img/portrait.svg" alt="" style="max-width:255px;">
                <h4>Arsène Lupin</h4>
                <p class="text-muted">My lab</p>
            </div>
        </a>
    </div>
</div>
