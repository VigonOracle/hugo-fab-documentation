+++
title = "Fab Lab ULB Theme"
hiddennav = true
fa = "pencil"
faVariant = "solid"
weight = 20
+++

## License

{{< license >}}


## About this template

This template was written by Nicolas H.-P. De Coster (Vigon) in 2023, originally for the [Fab Zero ULB](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website) course (as an alternative to [MkDocs](https://www.mkdocs.org/)) and the Fab Lab ULB website.

It was designed to be :
- easy to maintain by ["non-computer-people"](https://xkcd.com/627/)
- fast : [Hugo](https://gohugo.io/) was selected after an interesting discussion with [Kris](https://gitlab.com/kriwkrow) in Amsterdam during the [Fab Lab Instructor Bootcamp](http://academany.fabcloud.io/fabacademy/2023/instructors-bootcamp/)
- filled with features (see below)

This template is maintained inside : {{< templateLinks >}}.

## Features

This website template supports : 
- A [LaTeX-like](https://www.latex-project.org/) style largely inspired by [LaTeX Now](https://latex.now.sh/) and adapted by <abbr title="Nicolas H.-P. De Coster (Vigon)">NDC</abbr>, using the iconic [Latin Modern](https://www.gust.org.pl/projects/e-foundry/latin-modern/index_html) fonts family.
- [Bootstrap](https://getbootstrap.com/), an fast a responsive framework for building websites.
- [Font Awesome](https://fontawesome.com/), to integrate fancy and beautiful icons easily
- [jQuery](https://jquery.com/), a rich JavaScript library (used for the dropdown menu)
- [MathJax](https://www.mathjax.org/), to render LaTeX code in HTML (allows equations display)
- [QRCodeJS](https://davidshimjs.github.io/qrcodejs/) was used to generate QRCode on-the-fly on each page
- Direct HTML code in MarkDown documents


## Color code

Fab Lab ULB <span style="background-color:#ff581c;">orange(#ff581c)</span> is part of our identity, quite pushed by [Victor Levy](https://fabacademy.org/2018/labs/fablabulb/students/victor-levy/).

![](img/fablabULBHall.jpg)


## Contributors

- [Kris](https://gitlab.com/kriwkrow) was a great help to build the first skeleton (basic automated menu) and show me the first steps to get into [Hugo](https://gohugo.io/).
- The dropdown menu was inspired from https://bootsnipp.com/snippets/4qgR
- The "copy code" button was inspired by https://digitaldrummerj.me/hugo-add-copy-code-snippet-button/ and adapted by <abbr title="Nicolas H.-P. De Coster (Vigon)">NDC</abbr>.
