+++
title = "Hidden page"
hiddennav = true
weight = 20
+++

This page won't go in the navigation bar. (set variable hiddennav = true)

This can be usefull to link to a page in a document without having it as a link in the nav bar.
