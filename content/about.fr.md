+++
title = "About"
fa = "address-card"
faVariant = "regular"
weight = 10
+++

<div class="row">

  <div class="col-lg-3 m-auto">
    <img src="img/portrait.svg" width="300"><br>
  </div>

  <div class="col-lg-6 m-auto">

  Cette page en dit plus sur moi.

  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae placerat libero. Etiam non felis eros. Morbi aliquet ornare elementum. Maecenas sagittis enim mauris, eu cursus metus efficitur vel. Donec tempus purus ut est malesuada tempor. Donec a magna quis ex ultrices rhoncus. Aenean congue justo at dolor semper sollicitudin. Fusce faucibus leo vitae lectus tristique ullamcorper. Fusce dictum odio nec lacus suscipit, quis euismod nisi venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam non lacinia felis, nec egestas nulla. Etiam condimentum neque ut volutpat feugiat. Aliquam enim dui, lobortis luctus consequat non, venenatis quis tortor. Proin diam est, condimentum lacinia ornare vel, condimentum nec libero. Curabitur iaculis sem pretium commodo ultricies. Sed et commodo nisi, ultricies varius velit.

  Praesent quis tortor eu ante efficitur hendrerit. Donec est eros, rhoncus fringilla leo in, maximus varius arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed sagittis risus est, quis varius turpis posuere eu. Etiam egestas dui id aliquam dictum. In lectus lorem, mattis at varius eu, vehicula id eros. Vestibulum id vulputate neque. Fusce rhoncus mattis dictum. Duis sagittis urna sed sapien venenatis auctor. Nulla ullamcorper egestas augue, eu auctor odio placerat et. Nam nec laoreet mauris.
  </div>

</div>
